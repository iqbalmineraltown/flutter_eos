# flutter_eos

Experimental Architecture Boilerplate:

- BLoC per page
- Bind Stateful Widget and BLoC lifecycle
- export each modules into single file

Heavily inspired by [MVVMCross](https://github.com/MvvmCross/MvvmCross/)

## Structure

```markdown
lib
|
|
|--blocs
|    |--core
|    |--src
|    |--blocs.dart
|
|--models
|    |--core
|    |--src
|    |--models.dart
|
|--services
|    |--core
|    |--src
|    |--services.dart
|
|--widgets
|    |--core
|
|--main.dart
|--service_locator.dart

```

`services`: any backend-related logic

## Development

- run `flutter packages get`
- run `flutter pub run build_runner build`
  - run this whenever changes made on:
    - `lib/routers/`
    - `lib/models/`
