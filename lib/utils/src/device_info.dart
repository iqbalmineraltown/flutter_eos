import 'dart:io';

import 'package:device_info/device_info.dart';

class DeviceInfo {
  static DeviceInfo _instance;
  DeviceInfo get instance => _instance != null
      ? _instance
      : () {
          _instance = DeviceInfo();
          return _instance;
        };

  DeviceInfoPlugin _deviceInfoPlugin = DeviceInfoPlugin();
  AndroidDeviceInfo _androidDeviceInfo;
  IosDeviceInfo _iosDeviceInfo;

  Future initialize() async {
    if (Platform.isIOS) {
      _iosDeviceInfo = await _deviceInfoPlugin.iosInfo;
    } else if (Platform.isAndroid) {
      _androidDeviceInfo = await _deviceInfoPlugin.androidInfo;
    }
  }

  String osName() {
    if (Platform.isIOS) {
      return 'ios';
    }
    if (Platform.isAndroid) {
      return 'android';
    }
    return 'unknown';
  }

  String osVersion() {
    if (Platform.isIOS) {
      return _iosDeviceInfo.systemVersion;
    }
    if (Platform.isAndroid) {
      return _androidDeviceInfo.version.release;
    }
    return 'unknown';
  }

  String deviceModel() {
    if (Platform.isIOS) {
      return _iosDeviceInfo.utsname.machine;
    }
    if (Platform.isAndroid) {
      return "${_androidDeviceInfo.brand} ${_androidDeviceInfo.model} (${_androidDeviceInfo.device})";
    }
    return 'unknown';
  }
}
