import 'package:flutter/material.dart';
import 'package:flutter_eos/service_locator.dart';
import 'package:flutter_eos/widgets/app.dart';

void main() {
  initSingletonAPI();
  initBlocFactory();
  runApp(MyApp());
}
