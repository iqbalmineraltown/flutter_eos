import 'package:flutter_eos/blocs/blocs.dart';
import 'package:flutter_eos/services/core/local_resources.dart';
import 'package:flutter_eos/services/services.dart';
import 'package:get_it/get_it.dart';

var serviceLocator = GetIt.instance;

///Register Factory Bloc
void initBlocFactory() {
  serviceLocator.registerFactory<HomeBloc>(() => HomeBloc());
  serviceLocator.registerFactory<SecondBloc>(() => SecondBloc());
  serviceLocator.registerFactory<PokemonBloc>(() => PokemonBloc());
  serviceLocator.registerFactory<PokemonCachedBloc>(() => PokemonCachedBloc());
}

void initSingletonAPI() {
  serviceLocator.registerSingleton<CustomLocalResource>(FlutterLocalResource());
  serviceLocator.registerSingleton<PokemonRepo>(PokemonRepo());
}
