import 'dart:async';

import 'package:flutter_eos/blocs/core/base_bloc.dart';
import 'package:flutter_eos/utils/utils.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc implements BaseBloc {
  BehaviorSubject<int> _counterCtrl = BehaviorSubject<int>();
  Stream<int> get counterStream => _counterCtrl.stream;

  BehaviorSubject<String> _osNameCtrl = BehaviorSubject<String>();
  Stream<String> get osNameStream => _osNameCtrl.stream;

  BehaviorSubject<String> _osVersionCtrl = BehaviorSubject<String>();
  Stream<String> get osVersionStream => _osVersionCtrl.stream;

  BehaviorSubject<String> _modelCtrl = BehaviorSubject<String>();
  Stream<String> get modelStream => _modelCtrl.stream;

  @override
  Future initialize() async {
    print('initialize from ${this.runtimeType.toString()}');
    reset();

    var deviceInfoPlugin = DeviceInfo();
    await deviceInfoPlugin.initialize();
    _osNameCtrl.add(deviceInfoPlugin.osName());
    _osVersionCtrl.add(deviceInfoPlugin.osVersion());
    _modelCtrl.add(deviceInfoPlugin.deviceModel());
  }

  void increment() {
    _counterCtrl.value++;
  }

  void reset() {
    _counterCtrl.value = 0;
  }

  @override
  void dispose() {
    _counterCtrl.close();
    print('dispose from ${this.runtimeType.toString()}');
  }
}
