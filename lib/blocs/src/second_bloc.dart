import 'dart:async';

import 'package:flutter_eos/blocs/core/base_bloc.dart';
import 'package:rxdart/rxdart.dart';

class SecondBloc implements BaseBloc {
  BehaviorSubject<int> _counterCtrl = BehaviorSubject<int>();
  Stream<int> get counterStream => _counterCtrl.stream;

  @override
  Future initialize() async {
    print('initialize from ${this.runtimeType.toString()}');
  }

  void setPlaceholder(int data) async {
    _counterCtrl.value = data;
  }

  void increment() {
    _counterCtrl.value++;
  }

  @override
  void dispose() {
    print('dispose from ${this.runtimeType.toString()}');
    _counterCtrl.close();
  }
}
