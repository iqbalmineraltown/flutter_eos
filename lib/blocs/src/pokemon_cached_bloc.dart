import 'package:flutter_eos/blocs/core/base_bloc.dart';
import 'package:flutter_eos/models/models.dart';
import 'package:flutter_eos/services/services.dart';
import 'package:flutter_eos/service_locator.dart';
import 'package:rxdart/rxdart.dart';

class PokemonCachedBloc extends BaseBloc {
  PokemonRepo _pokemonRepo = serviceLocator<PokemonRepo>();

  BehaviorSubject<List<Pokemon>> _pokemonListCtrl =
      BehaviorSubject<List<Pokemon>>();
  Stream<List<Pokemon>> get pokemonStream => _pokemonListCtrl.stream;

  @override
  Future<void> initialize() async {
    var pokemonList = await _pokemonRepo.getAllCached();
    _pokemonListCtrl.add(pokemonList);
  }

  Future<void> refresh() async {
    var pokemonList = await _pokemonRepo.getAllCached(forceReload: true);
    _pokemonListCtrl.add(pokemonList);
  }

  @override
  void dispose() {
    _pokemonListCtrl.close();
  }
}
