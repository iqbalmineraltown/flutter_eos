import 'package:flutter/material.dart';
import 'package:flutter_eos/blocs/blocs.dart';
import 'package:flutter_eos/models/models.dart';
import 'package:flutter_eos/widgets/core/core_widgets.dart';

class PokemonPage extends BaseStatefulWidget<PokemonBloc> {
  @override
  _PokemonPageState createState() => _PokemonPageState();
}

class _PokemonPageState extends BaseState<PokemonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pokemon List'),
      ),
      body: RefreshIndicator(
        onRefresh: widget.bloc.refresh,
        child: Center(
          child: StreamStateBuilder<List<Pokemon>>(
            stream: widget.bloc.pokemonStream,
            builder: (context, snapshot, state) {
              switch (state) {
                case StreamState.success:
                  return Container(
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) => Card(
                        child: ListTile(
                          title: Text(
                            snapshot.data[index].name,
                          ),
                          subtitle: Text(snapshot.data[index].url),
                        ),
                      ),
                    ),
                  );
                case StreamState.error:
                  return Center(
                    child: Text('Error Occured'),
                  );
                case StreamState.loading:
                default:
                  return Center(
                    child: Text('Loading...'),
                  );
              }
            },
          ),
        ),
      ),
    );
  }
}
