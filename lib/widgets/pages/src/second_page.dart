import 'package:flutter/material.dart';
import 'package:flutter_eos/blocs/blocs.dart';
import 'package:flutter_eos/widgets/core/core_widgets.dart';

class SecondPage extends BaseStatefulWidget<SecondBloc> {
  final int data;
  SecondPage({Key key, this.data}) : super(key: key);

  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends BaseState<SecondPage> {
  @override
  void initState() {
    super.initState();
    widget.bloc.setPlaceholder(widget.data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Prev: ${widget.data}'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StreamBuilder<int>(
              stream: widget.bloc.counterStream,
              builder: (context, snapshot) {
                var text = 'Empty';
                if (snapshot.hasData && !snapshot.hasError) {
                  text = snapshot.data.toString();
                }
                return Text('Counter: $text');
              },
            ),
            StreamBuilder<int>(
              stream: widget.bloc.counterStream,
              builder: (context, snapshot) {
                var isDataAvailable = snapshot.hasData && !snapshot.hasError;
                return FlatButton(
                  child: Text('Open Again'),
                  color: Colors.black12,
                  onPressed: isDataAvailable
                      ? () => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (BuildContext context) {
                                  return SecondPage(data: snapshot.data);
                                },
                                fullscreenDialog: true),
                          )
                      : null,
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: widget.bloc.increment,
        tooltip: 'Increment',
        child: Icon(Icons.plus_one),
      ),
    );
  }
}
