import 'package:flutter/material.dart';
import 'package:flutter_eos/blocs/blocs.dart';
import 'package:flutter_eos/routers/router.gr.dart';
import 'package:flutter_eos/widgets/core/core_widgets.dart';

class HomePage extends BaseStatefulWidget<HomeBloc> {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends BaseState<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Arch Demo'),
      ),
      body: Center(
        child: StreamBuilder<int>(
          stream: widget.bloc.counterStream,
          builder: (context, snapshot) {
            var count = 0;

            var isDataAvailable = snapshot.hasData && !snapshot.hasError;
            if (isDataAvailable) {
              count = snapshot.data;
            }

            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('How Many?'),
                Text(
                  '$count',
                  key: const Key('counter'),
                ),
                FlatButton(
                  key: UniqueKey(),
                  child: Text('Reset'),
                  color: Colors.black12,
                  onPressed: () => widget.bloc.reset(),
                ),
                FlatButton(
                  key: UniqueKey(),
                  child: Text('Open Second Page'),
                  color: Colors.black12,
                  onPressed: isDataAvailable
                      ? () => Navigator.of(context).pushNamed(
                          AppRouter.secondPageRouter,
                          arguments: SecondPageArguments(data: snapshot.data))
                      : null,
                ),
                FlatButton(
                    key: UniqueKey(),
                    child: Text('Pokemon List'),
                    color: Colors.black12,
                    onPressed: () => Navigator.of(context)
                        .pushNamed(AppRouter.pokemonPageRouter)),
                FlatButton(
                    key: UniqueKey(),
                    child: Text('Pokemon Cached List'),
                    color: Colors.black12,
                    onPressed: () => Navigator.of(context)
                        .pushNamed(AppRouter.pokemonCachedPageRouter)),
                StreamBuilder<String>(
                  stream: widget.bloc.osNameStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData && !snapshot.hasError) {
                      return Text("OS: ${snapshot.data}");
                    }
                    return Container();
                  },
                ),
                StreamBuilder<String>(
                  stream: widget.bloc.osVersionStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData && !snapshot.hasError) {
                      return Text("OS Version: ${snapshot.data}");
                    }
                    return Container();
                  },
                ),
                StreamBuilder<String>(
                  stream: widget.bloc.modelStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData && !snapshot.hasError) {
                      return Text("Model: ${snapshot.data}");
                    }
                    return Container();
                  },
                ),
              ],
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        key: const Key('increment'),
        onPressed: widget.bloc.increment,
        tooltip: 'Increment',
        child: Icon(Icons.plus_one),
      ),
    );
  }
}
