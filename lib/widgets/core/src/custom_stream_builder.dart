import 'package:flutter/material.dart';

enum StreamState { success, error, loading }

typedef StreamStateWidgetBuilder<T> = Widget Function(
    BuildContext context, AsyncSnapshot<T> snapshot, StreamState state);

class StreamStateBuilder<T> extends StatelessWidget {
  final Stream<T> stream;
  final T initialData;
  final StreamStateWidgetBuilder<T> builder;

  StreamStateBuilder({
    Key key,
    this.initialData,
    @required this.stream,
    @required this.builder,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<T>(
      key: key,
      initialData: initialData,
      stream: stream,
      builder: (context, snapshot) {
        // Success
        if (snapshot.hasData && !snapshot.hasError) {
          return builder(context, snapshot, StreamState.success);
        }
        // Error
        if (snapshot.hasError) {
          return builder(context, snapshot, StreamState.error);
        }
        // Loading/Placeholder
        return builder(context, snapshot, StreamState.loading);
      },
    );
  }
}
