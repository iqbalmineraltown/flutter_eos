import 'package:flutter/material.dart';
import 'package:flutter_eos/blocs/core/base_bloc.dart';
import 'package:flutter_eos/service_locator.dart' as locator;

abstract class BaseStatefulWidget<T extends BaseBloc> extends StatefulWidget {
  final T bloc = locator.serviceLocator<T>();

  @mustCallSuper
  BaseStatefulWidget({Key key}) : super(key: key);
}

abstract class BaseState<T extends BaseStatefulWidget> extends State<T> {
  @mustCallSuper
  @override
  void initState() {
    super.initState();
    widget.bloc.initialize();
  }

  @mustCallSuper
  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }
}
