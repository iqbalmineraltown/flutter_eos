///Register all models here

export 'src/data.dart';
export 'src/pokemon.dart';
export 'src/serializers/pokemon_serializers.dart';
