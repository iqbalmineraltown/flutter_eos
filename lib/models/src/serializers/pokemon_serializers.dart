import 'package:flutter_eos/models/models.dart';
import 'package:jaguar_serializer/jaguar_serializer.dart';

part 'pokemon_serializers.jser.dart';

@GenSerializer(nameFormatter: toSnakeCase)
class PokemonJsonSerializer extends Serializer<Pokemon>
    with _$PokemonJsonSerializer {}
