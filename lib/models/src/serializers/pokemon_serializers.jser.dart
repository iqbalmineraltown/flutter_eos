// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_serializers.dart';

// **************************************************************************
// JaguarSerializerGenerator
// **************************************************************************

abstract class _$PokemonJsonSerializer implements Serializer<Pokemon> {
  @override
  Map<String, dynamic> toMap(Pokemon model) {
    if (model == null) return null;
    Map<String, dynamic> ret = <String, dynamic>{};
    setMapValue(ret, 'name', model.name);
    setMapValue(ret, 'url', model.url);
    return ret;
  }

  @override
  Pokemon fromMap(Map map) {
    if (map == null) return null;
    final obj = Pokemon(
        name: map['name'] as String ?? getJserDefault('name'),
        url: map['url'] as String ?? getJserDefault('url'));
    return obj;
  }
}
