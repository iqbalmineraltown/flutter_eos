class Pokemon {
  final String name;
  final String url;

  const Pokemon({this.name, this.url});
}
