import 'package:async_resource/async_resource.dart';
import 'package:async_resource_flutter/async_resource_flutter.dart';

///
abstract class CustomLocalResource {
  LocalResource getResourceInstance(String key);
}

class FlutterLocalResource implements CustomLocalResource {
  @override
  LocalResource<String> getResourceInstance(String key) {
    return StringMmkvResource(key, saveLastModified: true);
  }
}
