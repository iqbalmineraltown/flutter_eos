abstract class BaseHttpException implements Exception {
  final String message;

  BaseHttpException({this.message});

  String toString() {
    if (message == null) return "Exception";
    return "Exception: $message";
  }
}

/// Standardized non-success response object from backend
class NonSuccessResponseException extends BaseHttpException {
  NonSuccessResponseException(
    this.code,
    this.status,
    this.detail, {
    String message,
  }) : super(message: message);

  int code;
  String status;
  String detail;
}

class RequestTimeoutException extends BaseHttpException {}
