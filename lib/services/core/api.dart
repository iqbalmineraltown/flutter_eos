import 'dart:async';
import 'dart:convert';
import 'package:meta/meta.dart';
import 'package:http/http.dart';

import 'package:async_resource/async_resource.dart';
import 'package:async_resource_flutter/async_resource_flutter.dart';

import 'package:flutter_eos/service_locator.dart';

import 'exceptions.dart';
import 'local_resources.dart';

/// HTTP Request method as enum for integrity
enum HttpMethod {
  get,
  post,
  delete,
  put,
}

/// interpret everything below this as success response
const int minimumNonSuccessCode = 400;

/// Base class for http request with helper
abstract class BaseHttpRequest {
  static int _requestId = 0;
  Client get client => Client();

  String get baseUrl;

  int get requestId => _requestId++;

  /// set default header here
  Map<String, String> get baseHeaders => {
        'Content-Type': 'application/json',
      };

  Uri generateUri(
    String url,
    String path,
    Map<String, String> params,
  ) {
    var apiUri = Uri.parse("$url$path");

    /// prevent http caching
    params ??= <String, String>{};
    params['timestamp'] = DateTime.now().millisecondsSinceEpoch.toString();

    return Uri(
      scheme: apiUri.scheme,
      host: apiUri.host,
      path: apiUri.path,
      queryParameters: params,
    );
  }

  void preRequestLog(int requestId, Stopwatch timer, String logMessage,
      {dynamic body}) {
    assert(() {
      timer.start();
      print(logMessage);
      return true;
    }());
  }

  void postResponseLog(int requestId, Stopwatch timer, String logMessage) {
    assert(() {
      timer.stop();
      var elapsedMiliseconds = timer.elapsedMilliseconds;
      print(logMessage);
      print("[$requestId] < Took ${elapsedMiliseconds}ms");
      return true;
    }());
  }

  void nonSuccessHandler(Response response) {
    if (response.statusCode >= minimumNonSuccessCode) {
      Map res = json.decode(utf8.decode(response.bodyBytes));

      /// add specific error code handling here
      /// precedence matters, [NonSuccessResponseException]
      /// should be the last and default handler

      var errorObj = NonSuccessResponseException(
        response.statusCode,
        res['status'],
        res['detail'],
      );
      throw errorObj;
    }
  }
}

/// Generic, multipurpose http request
abstract class HttpRequest extends BaseHttpRequest {
  Future<String> sendRequest(HttpMethod requestMethod, String relativePath,
      {dynamic body,
      Map<String, dynamic> params,
      Map<String, String> headers,
      bool isUrlEncoded = false,
      Duration timeout = const Duration(seconds: 20)}) async {
    assert(requestMethod != HttpMethod.post ||
        (requestMethod == HttpMethod.post &&
            body != null &&
            (body is String || body is List || body is Map)));

    var httpMethod = requestMethod.toString().split(".")[1].toUpperCase();

    var uri = generateUri(baseUrl, relativePath, params);

    var request = Request(httpMethod, uri);

    /// copy default headers first then override value
    var combinedHeaders = baseHeaders;
    if (headers != null) {
      combinedHeaders.addAll(headers);
    }
    request.headers.addAll(combinedHeaders);

    if (body != null) {
      if (!isUrlEncoded) {
        body = json.encoder.convert(body);
      }

      if (body is String) {
        request.body = body;
      } else if (body is List) {
        request.bodyBytes = List.castFrom(body);
      } else if (body is Map) {
        /// headers 'Content-Type' will be overriden
        /// see https://github.com/dart-lang/http/blob/ffe786a872c0d443cc4fa6baa923d69059b66399/lib/src/client.dart#L60-L62
        request.bodyFields = Map.castFrom(body);
      } else {
        throw ArgumentError('Invalid request body type "${body.toString()}".');
      }
    }

    var timer = Stopwatch();
    var currentRequestId = requestId;
    preRequestLog(currentRequestId, timer,
        "[$currentRequestId] > $httpMethod ${uri.toString()}");
    if (request.body != null) {
      preRequestLog(currentRequestId, timer,
          "[$currentRequestId] > ${request.body.toString()}");
    }

    var req = await client.send(request).timeout(timeout, onTimeout: () {
      postResponseLog(
          requestId, timer, "[$requestId] < Timeout after ${timeout}s!");
      return;
    });

    var response = await Response.fromStream(req);

    postResponseLog(currentRequestId, timer,
        "[$currentRequestId] < ${response.statusCode} ${response.body}");

    nonSuccessHandler(response);

    return utf8.decode(response.bodyBytes);
  }

  String _generateCacheKey(Uri uri) {
    /// remove params
    return uri.toString().split('?')[0].replaceAll("/", "|");
  }

  Future<String> getRequestWithCache(
    String relativePath, {
    Map<String, dynamic> params,
    Map<String, dynamic> headers,
    Duration timeout = const Duration(seconds: 60),
    bool isCacheFirst = true,
    bool forceReload = false,
    Duration maxCacheAge = const Duration(days: 1),
  }) async {
    var uri = generateUri(baseUrl, relativePath, params);

    var combinedHeaders = baseHeaders;
    if (headers != null) {
      combinedHeaders.addAll(headers);
    }

    CustomLocalResource localResource = serviceLocator<CustomLocalResource>();
    var myDataResource = _CustomHttpNetworkResource<String>(
      url: uri.toString(),
      headers: combinedHeaders,
      client: client,
      binary: true,
      parser: (content) => content.toString(),

      /// url parsing strategy for cache' key
      cache: localResource.getResourceInstance('${_generateCacheKey(uri)}'),
      maxAge: maxCacheAge,
      strategy:
          isCacheFirst ? CacheStrategy.cacheFirst : CacheStrategy.networkFirst,
      responseHandler: nonSuccessHandler,
    );

    var timer = Stopwatch();
    var currentRequestId = requestId;

    preRequestLog(currentRequestId, timer,
        "[$currentRequestId] > GET with cache ${uri.toString()}");

    if (myDataResource.headers != null) {
      preRequestLog(currentRequestId, timer,
          "[$currentRequestId] > Headers: ${myDataResource.headers}");
    }

    var response = await myDataResource
        .get(forceReload: forceReload)
        .timeout(timeout, onTimeout: () {
      postResponseLog(
          requestId, timer, "[$requestId] < Timeout after ${timeout}s!");
      throw RequestTimeoutException();
    });

    postResponseLog(currentRequestId, timer, "[$currentRequestId] < $response");
    return response;
  }
}

class _CustomHttpNetworkResource<T> extends NetworkResource<T> {
  _CustomHttpNetworkResource(
      {@required String url,
      @required LocalResource<T> cache,
      Duration maxAge,
      CacheStrategy strategy,
      Parser parser,
      this.client,
      this.headers,
      this.binary: false,
      @required this.responseHandler})
      : assert(binary != null),
        super(
            url: url,
            cache: cache,
            maxAge: maxAge,
            strategy: strategy,
            parser: parser);

  /// Optional. The [http.Client] to use, recommended if frequently hitting
  /// the same server. If not specified, [http.get()] will be used instead.
  final Client client;

  /// Optional. The HTTP headers to send with the request.
  final Map<String, String> headers;

  /// Whether the underlying data is binary or string-based.
  final bool binary;

  Function(Response) responseHandler;

  /// retrieve full response after get

  @override
  Future<dynamic> fetchContents() async {
    final response = await (client == null
        ? get(url, headers: headers)
        : client.get(url, headers: headers));

    responseHandler(response);

    return (response != null)
        ? (binary ? utf8.decode(response.bodyBytes) : response.body)
        : null;
  }
}
