import 'dart:convert';

import 'package:flutter_eos/models/models.dart';
import 'package:flutter_eos/services/core/api.dart';

class PokemonRepo extends HttpRequest {
  @override
  String get baseUrl => 'https://pokeapi.co/api/v2/';

  Future<List<Pokemon>> getAll() async {
    final response = await sendRequest(HttpMethod.get, 'pokemon');
    final Map<String, dynamic> results = json.decode(response);

    List<Pokemon> listPokemon = List<Pokemon>();
    results['results'].forEach(
        (element) => listPokemon.add(PokemonJsonSerializer().fromMap(element)));
    return listPokemon;
  }

  Future<List<Pokemon>> getAllCached({bool forceReload = false}) async {
    final response = await getRequestWithCache(
      'pokemon',
      forceReload: forceReload,
      maxCacheAge: const Duration(seconds: 30),
    );
    final Map<String, dynamic> results = json.decode(response);

    List<Pokemon> listPokemon = List<Pokemon>();
    results['results'].forEach(
        (element) => listPokemon.add(PokemonJsonSerializer().fromMap(element)));
    return listPokemon;
  }
}
