// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:flutter_eos/widgets/pages/src/home_page.dart';
import 'package:flutter_eos/widgets/pages/src/second_page.dart';
import 'package:flutter_eos/widgets/pages/src/pokemon_page.dart';
import 'package:flutter_eos/widgets/pages/src/pokemon_cached_page.dart';

class AppRouter {
  static const homePageRouter = '/';
  static const secondPageRouter = '/secondPageRouter';
  static const pokemonPageRouter = '/pokemonPageRouter';
  static const pokemonCachedPageRouter = '/pokemonCachedPageRouter';
  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<AppRouter>();
  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case homePageRouter:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => HomePage(key: typedArgs),
          settings: settings,
        );
      case secondPageRouter:
        if (hasInvalidArgs<SecondPageArguments>(args)) {
          return misTypedArgsRoute<SecondPageArguments>(args);
        }
        final typedArgs = args as SecondPageArguments ?? SecondPageArguments();
        return MaterialPageRoute(
          builder: (_) => SecondPage(key: typedArgs.key, data: typedArgs.data),
          settings: settings,
          fullscreenDialog: true,
        );
      case pokemonPageRouter:
        return MaterialPageRoute(
          builder: (_) => PokemonPage(),
          settings: settings,
        );
      case pokemonCachedPageRouter:
        return MaterialPageRoute(
          builder: (_) => PokemonCachedPage(),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//--------------------------------------------------------------------------
// Arguments holder classes
//---------------------------------------------------------------------------

//SecondPage arguments holder class
class SecondPageArguments {
  final Key key;
  final int data;
  SecondPageArguments({this.key, this.data});
}
