import 'package:auto_route/auto_route_annotations.dart';
import 'package:flutter_eos/widgets/pages/pages.dart';

@autoRouter
class $AppRouter {
  @initial
  HomePage homePageRouter;

  @MaterialRoute(fullscreenDialog: true)
  SecondPage secondPageRouter;

  PokemonPage pokemonPageRouter;

  PokemonCachedPage pokemonCachedPageRouter;
}
